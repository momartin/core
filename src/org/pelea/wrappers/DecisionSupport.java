/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.wrappers;

/**
 *
 * @author moises
 */
public interface DecisionSupport 
{
    public String RepairOrReplan(String stateH, String domainH, String problem, String planH);
    public String getPlanHInfoMonitor(String stateH, String domainH, String problem);
}
