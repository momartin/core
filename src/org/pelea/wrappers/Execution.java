package org.pelea.wrappers;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

public interface Execution 
{ 
    public void executePlan(String planL);

    public void executeAction(String actionL);
    
    public void executeAction(double time, String action);

    public String getSensors();
    
    public String getSensorsWithTime(double instant_time);
    
    public double getLastTime();
}
