/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.planner.plan;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author moises
 */
public class PlanAction 
{
    private String name;
    private List values; 
    private int execution_order;
    private double duration;
    private double cost;
    private double startTime;
    
    public PlanAction(String name, int order)
    {
        this.name = name;
        this.values = new ArrayList<String>();
        this.execution_order = order;
        this.duration = 0;
        this.startTime = 0;
        this.cost = 1;
    }
    
    public PlanAction(String name, int order, double duration)
    {
        this.name = name;
        this.values = new ArrayList<String>();
        this.execution_order = order;
        this.duration = duration;
        this.startTime = 0;
        this.cost = 1;
    }
    
    public PlanAction()
    {
        this.values = new ArrayList<String>();
        this.duration = 0;
        this.cost = 0;
        this.startTime = 0;
    }
    
    public PlanAction(String name, int order, double duration, double cost)
    {
        this.name = name;
        this.values = new ArrayList<String>();
        this.execution_order = order;
        this.duration = duration;
        this.cost = cost;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public void setName(String name)
    {
        this.name = name.toUpperCase();
    }
    
    public String getValue(int position)
    {
        return (String) this.values.get(position);
    }
    
    public void addValue(String value)
    {
        this.values.add(this.values.size(), value.toUpperCase());
    }
    
    public List getValues()
    {
        return this.values;
    }
    
    public double getDuration()
    {
        return this.duration;
    }
    
    public void setDuration(double duration)
    {
        this.duration = duration;
    }
    
    public double getCost()
    {
        return this.cost;
    }
    
    public void setCost(double cost)
    {
        this.cost = cost;
    }
    
    public int size()
    {
        return this.values.size();
    }
    
    public int getExecutionOrder()
    {
        return this.execution_order;
    }
    
    public void setExecutionOrder(int order)
    {
        this.execution_order = order;
    }
    
    public double getStartTime()
    {
        return this.startTime;
    }
    
    public void setStartTime(double time)
    {
        this.startTime = time;
    }
}
