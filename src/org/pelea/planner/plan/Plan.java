/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.planner.plan;

import java.util.ArrayList;
import java.util.List;
import org.pelea.utils.Util;
import xml2pddl.XML2PDDL;

/**
 *
 * @author moises
 */
public class Plan 
{
    private List<PlanAction> actions;
    private double time;
    private int cost;
    
    public Plan()
    {
        this.actions = new ArrayList();
        this.time    = 0;
        this.cost    = 0;
    }
    
    public Plan(double time) {
        this.actions = new ArrayList();
        this.time = time;
        this.cost = 0;
    }
    
    public void setTime(double time)
    {
        this.time = time;
    }
    
    public void setCost(int cost)
    {
        this.cost = cost;
    }
    
    public void addAction(PlanAction action)
    {
        this.actions.add(action);
    }
    
    public double getTime()
    {
        return this.time;
    }
    
    public int getCost()
    {
        return this.cost;
    }
    
    public PlanAction getAction(int position)
    {
        return this.actions.get(position);
    }
    
    public String getActionName(int position)
    {
        return this.actions.get(position).getName();
    }
    
    public String getActionValue(int action, int value)
    {
        return this.actions.get(action).getValue(value);
    }
    
    public int size()
    {
        return this.actions.size();
    }
    
    private int getNumberOfGroups()
    {
        return this.actions.get(this.actions.size()-1).getExecutionOrder();
    }
    
    public String generateXML(String domainH, String stateH)
    {
            
        String xmlAction        = "";
        String xmlPlan          = "";
            
        int group               = 0;
        int actionCounter       = 0;        
        
        try 
        {
            //TODO: OTRA PUTA MIERDA MAS DE CESAR, ESTO TENGO QUE CAMBIARLO ES INEFICIENTE A NIVELES CUANTICOS.
            String[] domainActions  = XML2PDDL.getActions(domainH).split("\\s+");
            String[] parameters     = XML2PDDL.getParameters(domainH).split("\\|");
            String[] params_action;
                
            String domain_name      = XML2PDDL.getNameDomain(domainH);
            String problem_name     = XML2PDDL.getNameProblem(stateH);
            
            xmlPlan += "<plan time=\"" + this.time + "\">";
            
            for (int i = 0; i < actions.size(); i++)
            {
                PlanAction action = this.actions.get(i);
             
                if (action.getExecutionOrder() != group)
                {
                    xmlPlan += "<action-plan num_actions=\"" + actionCounter + "\">";
                    xmlPlan += xmlAction;
                    xmlPlan += "</action-plan>";
                    
                    xmlAction = "";
                    actionCounter = 0;
                    
                    group++;
                }
                
                xmlAction += "<action name=\"" + action.getName() + "\" start_time=\"" + action.getStartTime() + "\" execution_time=\"" + action.getCost() + "\">";
                
                params_action = parameters[Util.getPosition(domainActions, action.getName())].split("\\s+");
               
                for (int j = 0; j < action.size(); j++)
                {
                   xmlAction += "<term name=\"" + action.getValue(j).toUpperCase() + "\" class=\"" + params_action[j].toUpperCase() + "\" type=\"pddl_variable\"/>";      
                }

                xmlAction += "</action>";
                
                actionCounter++;
            }
            
            xmlPlan += "<action-plan num_actions=\"" + actionCounter + "\">";
            xmlPlan += xmlAction;
            xmlPlan += "</action-plan>";

            xmlPlan += "</plan>";

        } 
        catch (Exception ex) 
        {
            xmlPlan = "<error>";
            xmlPlan += "<type>8</type>";
            xmlPlan += "<message>Error processing plan</message>";
            xmlPlan += "</error>";
        }
        
        return xmlPlan;

    }
}
