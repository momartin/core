/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.utils.options;

/**
 *
 * @author moises
 */
public class Option 
{
    private byte _type; 
    private String _value;
    private String _code;
    
    public Option(byte type)
    {
        this._type      = type;
    }
    
    public Option(byte type, String value, String code)
    {
        this._type  = type;
        this._value = value;
        this._code  = code;
    }
    
    public byte getType()
    {
        return this._type;
    }
    
    public String getCode()
    {
        return this._code;
    }
    
    public String getValue()
    {
        return this._value;
    }
    
    public void setValue(String value)
    {
        this._value = value;
    }
}
