/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.utils.experimenter;

/**
 *
 * @author moises
 */
public class Episode {
    private final double time;
    private final String actions;
    
    public Episode(String time, String actions) {
        this.time = Double.parseDouble(time);
        this.actions = actions;
    }
    
    public double getTime() {
        return this.time / 1000.0;
    }
    
    public double getTimeMiliseconds() {
        return this.time;
    }
    
    public String getActions() {
        return this.actions;
    }
    
    public String getXML() {
        String result;
        
        result  = "<EPISODE>";
        result += "<PLANINGTIME>" + this.getTime() + "</PLANINGTIME>";
        result += "<ACTIONS>" + this.actions + "</ACTIONS>";
        result += "</EPISODE>";
        
        return result;
    }
}
