/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.utils.experimenter.result;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.Element;

/**
 *
 * @author momartin
 */
public class ProblemNode {
    
    private final String name;
    private final List<PlannerNode> planners;

    public ProblemNode(String name) {
        this.name = name;
        this.planners = new ArrayList<PlannerNode>();
    }
    
    public String getName() {
        return this.name;
    }
    
    private PlannerNode getPlanner(int horizon) {
        for (PlannerNode planner : this.planners) {
            if (planner.getHorizon() == horizon) {
                return planner;
            }
        }
        return null;
    }
    
    public void addExperiment(Element node, int horizon) {
       
        PlannerNode planner = this.getPlanner(horizon);
        
        if (planner == null) {
            planner = new PlannerNode(horizon);
            planner.addExperiment(node);
            this.planners.add(planner);
        }
        else
            planner.addExperiment(node);
    }
    
    public String printData(int option, int horizon) {

        PlannerNode p = this.getPlanner(horizon);
        
        if (p != null) {
            switch (option) {
                case 1: return p.getFirstPlanningTime();
                case 2: return p.getPlanningTime();
                case 3: return p.getReplanningEpisodes();
                case 4: return p.getNumActions();
                case 5: return p.getExperiments();
            }
        }
        
        return "-";
    }
    
    private int getMax() {
        int max = 0;
        
        for (int i = 0; i < this.planners.size(); i++) {
            if (max < this.planners.get(i).getSize(0))
                max = this.planners.get(i).getSize(0);
        }
        
        return max;
    }
    
    public String generateName(int position) {
        if (this.planners.get(position).getHorizon() == 1000000)
            return "FD";
        else
            return "AKFD(" + this.planners.get(position).getHorizon() + ")";
    }
    
    public void generateGraph(int horizons) throws IOException {
        
        DecimalFormat structure = new DecimalFormat("0.00"); 
        
        List<Integer> vHorizon = new ArrayList<Integer>();
        int i = 0;
        
        if (horizons == this.planners.size()) {
            
            for (i = 0; i < this.planners.size(); i++) {
                vHorizon.add(this.planners.get(i).getHorizon());
            }
                
            BufferedWriter buffer = new BufferedWriter(new FileWriter("data_" + this.getName() + ".dat"));
            
            int max = this.getMax();
            double maxTime = 0.0;
            
            for (i = 0; i < max; i++) {
                
                buffer.write(i + " ");
                
                for (int j = 0; j < this.planners.size(); j++) {
                    
                    if (maxTime < this.planners.get(j).getEpisode(0, i))
                        maxTime = this.planners.get(j).getEpisode(0, i);
                    
                    buffer.write(structure.format(this.planners.get(j).getEpisode(0, i)).replace(",", ".") + " ");
                }
                
                buffer.newLine();
            }
            
            buffer.write(i + " ");
            
            for (i = 0; i < this.planners.size(); i++) {
                buffer.write("0.00 ");
            }
            
            buffer.close();
            
            Collections.sort(vHorizon);
            
            BufferedWriter gnu = new BufferedWriter(new FileWriter(this.getName() + ".gp"));
            
            gnu.write("set terminal pdf");
            gnu.newLine();
            gnu.write("set output '" + this.getName() + ".pdf'");
            gnu.newLine();
            gnu.write("set grid");
            gnu.newLine();
            gnu.write("set xlabel \"Planning iteration\"");
            gnu.newLine();
            gnu.write("set xrange [0:" + (max+5) + ".0]");
            gnu.newLine();
            gnu.write("set ylabel \"Planning Time (Seconds)\"");
            gnu.newLine();
            gnu.write("set yrange [0:" + (maxTime+5) + "]");
            gnu.newLine();
            gnu.write("set title \"Average planning time for the whole cycle of planning and execution\"");
            gnu.newLine();
            gnu.write("plot ");
            
            for (i = 0; i < vHorizon.size(); i++) {
                for (int j = 0; j < this.planners.size(); j++) {
                    if (vHorizon.get(i) == this.planners.get(j).getHorizon()) {
                        if ((i+1) == vHorizon.size())
                            gnu.write("\"data_" + this.getName() + ".dat\" using 1:" + (j+2) + " with lines title \"" + this.generateName(j) + "\" ");
                        else    
                            gnu.write("\"data_" + this.getName() + ".dat\" using 1:" + (j+2) + " with lines title \"" + this.generateName(j) + "\", \\");
                        gnu.newLine();
                        break;
                    }
                }
            }
            
            gnu.close();
            
            BufferedWriter execution = new BufferedWriter(new FileWriter(this.getName() + ".sh"));

            execution.write("#! /bin/bash");
            execution.newLine();
            execution.write("gnuplot " + this.getName() + ".gp");
            execution.newLine();
            
            execution.close();

            Runtime rt   = Runtime.getRuntime();
            Process p    = rt.exec("sh " + this.getName() + ".sh");
            try {
                int exitVal = p.waitFor();

            } catch (InterruptedException ex) {
                Logger.getLogger(ProblemNode.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            //new File("data_" + this.getName() + ".dat").delete(); 
            //new File(this.getName() + ".gp").delete();     
            new File(this.getName() + ".sh").delete();
        }
    }
}
