/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.utils.experimenter.result;

import java.text.DecimalFormat;

/**
 *
 * @author moises
 */
public class EpisodeNode {
    private final double time;
    private final int actions;
    
    public EpisodeNode(double time, int actions) {
        this.time = time;
        this.actions = actions;
    }
    
    public double getTime() {
        return this.time;
    }
    
    public int getActions() {
        return this.actions;
    }
}
