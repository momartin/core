/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.utils.experimenter.result;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;

/**
 *
 * @author momartin
 */
public class PlannerNode {
    
    private final int horizon;
    private final List<ExperimentNode> experiments;
    //private final List<Problem> problems;
    
    public PlannerNode(int horizon) {
        this.horizon = horizon;
        this.experiments = new ArrayList<ExperimentNode>();
    }
    
    public int getHorizon() {
        return this.horizon;
    }
    
    public void addExperiment(Element node) {
        
        ExperimentNode exp;
        
        if (node.getChild("Horizon") != null) {
            exp = new ExperimentNode(Double.parseDouble(node.getChild("TOTALTIME").getValue()), 
                                Integer.parseInt(node.getChild("REPLANING").getValue()), 
                                Integer.parseInt(node.getChild("EXECUTEDACTIONS").getValue()),
                                Double.parseDouble(node.getChild("FIRSTPLANNINGTIME").getValue()),
                                Integer.parseInt(node.getChild("Horizon").getValue()),
                                Integer.parseInt(node.getChild("RESULT").getValue()));
        }
        else {
            exp = new ExperimentNode(Double.parseDouble(node.getChild("TOTALTIME").getValue()), 
                                Integer.parseInt(node.getChild("REPLANING").getValue()), 
                                Integer.parseInt(node.getChild("EXECUTEDACTIONS").getValue()),
                                Double.parseDouble(node.getChild("FIRSTPLANNINGTIME").getValue()),
                                Integer.parseInt(node.getChild("RESULT").getValue()));
        }
        
        List<Element> episodes = node.getChild("EPISODES").getChildren();
    
        for (int i = 0; i < episodes.size(); i++) {
            exp.addEpisode(Double.parseDouble(episodes.get(i).getChild("PLANINGTIME").getValue()), Integer.parseInt(episodes.get(i).getChild("ACTIONS").getValue()));
        }
        
        this.experiments.add(exp);
    }
    
    public String getPlanningTime() {
        
        DecimalFormat structure = new DecimalFormat("0.00"); 
        
        double mean = 0.0;
        double average = 0.0;
        
        for (int i = 0; i < this.experiments.size(); i++) {
            mean += this.experiments.get(i).getPlanningTime();
            System.out.print(this.experiments.get(i).getPlanningTime() + " + ");
        }
        
        mean = mean / this.experiments.size();
        
        for (int i = 0; i < this.experiments.size(); i++) {
            average += Math.pow((this.experiments.get(i).getPlanningTime() - mean), 2);
        }
        
        average = Math.sqrt(average / this.experiments.size());
        
        return "$" + structure.format(mean) + " \\pm " + structure.format(average) + "$ ";
    }
    
    public String getFirstPlanningTime() {
        
        DecimalFormat structure = new DecimalFormat("0.00"); 
        
        double mean = 0.0;
        double average = 0.0;
        
        for (int i = 0; i < this.experiments.size(); i++) {
            mean += this.experiments.get(i).getFirstPlanningTime();
        }
        
        mean = mean / this.experiments.size();
        
        for (int i = 0; i < this.experiments.size(); i++) {
            average += Math.pow(this.experiments.get(i).getFirstPlanningTime() - mean, 2);
        }
        
        average = Math.sqrt(average / this.experiments.size());
        
        return "$" + structure.format(mean) + " \\pm " + structure.format(average) + "$ ";
    }
    
    public String getNumActions() {
        
        DecimalFormat structure = new DecimalFormat("0.00"); 
        
        double mean = 0.0;
        double average = 0.0;
        
        for (int i = 0; i < this.experiments.size(); i++) {
            mean += this.experiments.get(i).getNumActions();
        }
        
        mean = mean / this.experiments.size();
        
        for (int i = 0; i < this.experiments.size(); i++) {
            average += Math.pow(this.experiments.get(i).getNumActions() - mean, 2);
        }
        
        average = Math.sqrt(average / this.experiments.size());
        
        return "$" + structure.format(mean) + " \\pm " + structure.format(average) + "$ ";
    }
    
    public String getReplanningEpisodes() {
        
        DecimalFormat structure = new DecimalFormat("0.00"); 
        
        double mean = 0.0;
        double average = 0.0;
        
        for (int i = 0; i < this.experiments.size(); i++) {
            mean += this.experiments.get(i).getReplanningEpisodes();
        }
        
        mean = mean / this.experiments.size();
        
        for (int i = 0; i < this.experiments.size(); i++) {
            average += Math.pow(this.experiments.get(i).getReplanningEpisodes() - mean, 2);
        }
        
        average = Math.sqrt(average / this.experiments.size());
        
        return "$" + structure.format(mean) + " \\pm " + structure.format(average) + "$ ";
    }
    
    public String getExperiments() {
        return "$" + this.experiments.size() + "$ ";
    }
    
    public int getSize(int position) {
        return this.experiments.get(position).getNumEpisodes();
    }
    
    public double getEpisode(int position, int episode) {
        return this.experiments.get(position).getEpisodeTime(episode);
    }
    
    public double getEpisodeMaxTime(int episode) {
        double time = 0.0;
        
        for (int i = 0; i < this.experiments.size(); i++) {
            if (this.experiments.get(i).getEpisodeTime(episode) > time)
                time = this.experiments.get(i).getEpisodeTime(episode);
        }
        return time;
    }
}
