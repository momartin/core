/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.utils.experimenter.result;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author momartin
 */
public class ExperimentNode {
    
    private final int replanning;
    private final int actions;
    private final int result;
    private final int horizon;
    private final double time;
    private final double firstTime;
    private final List<EpisodeNode> episodes;
    
    public ExperimentNode(double time, int replanning, int actions, double firstTime, int horizon, int result) {
        this.replanning = replanning;
        this.actions = actions;
        this.horizon = horizon;
        this.result = result;
        this.time = time;
        this.firstTime = firstTime;
        this.episodes = new ArrayList<EpisodeNode>();
    }
    
    public ExperimentNode(double time, int replanning, int actions, double firstTime, int result) {
        this.replanning = replanning;
        this.actions = actions;
        this.result = result;
        this.time = time;
        this.firstTime = firstTime;
        this.horizon = 10000000;
        this.episodes = new ArrayList<EpisodeNode>();  
    }
    
    public void addEpisode(double time, int actions) {
        this.episodes.add(new EpisodeNode(time, actions));
    }
    
    public double getPlanningExecutionTime() {
        return this.time;
    }
    
    public double getFirstPlanningTime() {
        return this.firstTime;
    }
    
    public int getReplanningEpisodes() {
        return this.replanning;
    }
    
    public int getNumActions() {
        return this.actions;
    }
    
    public int getHorizon() {
        return this.horizon;
    }
    
    public int getNumEpisodes() {
        return this.episodes.size();
    }
    
    public double getEpisodeTime(int episode) {
        
        if (episode >= this.episodes.size())
            return 0.00;
        else
            return this.episodes.get(episode).getTime();
    }
    
    public double getPlanningTime() {
        double sum = 0.0;
        
        for (EpisodeNode episode : this.episodes) {
            sum += episode.getTime();
        }
        return sum;
    }
}
