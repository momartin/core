/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.utils.experimenter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author moises
 */
public class ExperimentPool {
    
    public String name;
    public List<Experiment> experiments;
    
    public ExperimentPool(String name) {
        this.name = name;
        this.experiments = new ArrayList<Experiment>();
    }
    
    public void addExperiment(Experiment e) {
        this.experiments.add(e);
    }
    
    public void addValue(int position, String value, String name) {
        this.experiments.get(position).addValue(name, value);
    }
    
    public void executeAction() {
        this.experiments.get(this.experiments.size()-1).executeAction();
    }
    
    public void finishExperiment(int state) {
        this.experiments.get(this.experiments.size()-1).finish(state);
    }
    
    public void addEpisode(String time, String actions) {
        this.experiments.get(this.experiments.size()-1).addEpisode(time, actions);
    }
    
    public void saveXML(String fileName, String domain, String problem) throws IOException {
        
        BufferedWriter buffer;
        
        int totalActions = 0;
        int totalReplanning = 0;
        double totalTime = 0;
        double totalFirstPlanningTime = 0;
                        
        boolean exists = new File(fileName).exists();
        
        if (!exists) {
            buffer = new BufferedWriter(new FileWriter (fileName, false));
            buffer.write("<?xml version=\"1.0\"?>");
        }
        else {
            buffer = new BufferedWriter(new FileWriter (fileName, true));
        }
        
        buffer.write("<EXPERIMENTS>");

        int episodes = this.experiments.size();

        for (int i = 0; i < episodes; i++) {

            Experiment e = this.experiments.get(i);

            buffer.write(e.getXML(domain, problem, this.name));

            totalActions += e.getNumActions();
            totalTime += e.getTime();
            totalFirstPlanningTime += e.getPlanningTime(0);
            totalReplanning += e.getReplanningEpisodes();
         }

         buffer.write("<GLOBAL><ACTION>" + totalActions/episodes + "</ACTION><FIRST>" + totalFirstPlanningTime/episodes + "</FIRST><TIME>" + totalTime/episodes + "</TIME><REPLANING>" + totalReplanning/episodes + "</REPLANING></GLOBAL>");
         buffer.write("</EXPERIMENTS>");
         buffer.close();
         
    }
}
