/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.utils.experimenter;

/**
 *
 * @author moises
 */
public class Value {
    
    private final String name;
    private final String value;
    
    public Value(String name, String value) {
        this.name = name;
        this.value = value;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public String getXML() {
        return "<" + this.name + ">" + this.value + "</" + this.name + ">";
    }
    
}
