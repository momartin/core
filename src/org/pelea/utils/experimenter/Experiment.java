/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.utils.experimenter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.pelea.core.configuration.configuration;

/**
 *
 * @author moises
 */
public class Experiment {
    
    public static final int SOLVED = 1;
    public static final int DEADEND = 2;
    public static final int ERROR = 3;
    public static final int OUTOFTIME = 4;
    public static final int BLOCK = 5;
    
    public String SEPARATOR = ";";
    
    private final long time_start;
    private long time_end;
    private int replanning;
    private int executedActions;
    private int result;
    
    private final List<Episode> planningEpisodes;
    private final List<Value> values;
    
    public Experiment() {
        
        this.SEPARATOR = configuration.getInstance().getParameter("GENERAL", "SEPARATOR");
        this.time_start = System.currentTimeMillis();
        this.time_end = 0;
        this.replanning = -1;
        this.executedActions = 0;
        this.planningEpisodes = new ArrayList<Episode>();
        this.values = new ArrayList<Value>();
    }
    
    private String getValue(String name) {
        for (int i = 0; i < this.values.size(); i++) {
            if (this.values.get(i).getName().toUpperCase().contains(name)) {
                return this.values.get(i).getValue();
            }
        }
        
        return null;
    }
    
    public void finish(int result) {
        this.result = result;
        this.time_end = System.currentTimeMillis();
    }
    
    public void addEpisode(String time, String actions) {
        this.planningEpisodes.add(new Episode(time, actions));
        this.replanning++;
    }
    
    public void addValue(String name, String value) {
        this.values.add(new Value(name, value));
    }
    
    public void addValue(Value value) {
        this.values.add(value);
    }

    public void executeAction() {
        this.executedActions++;
    }
    
    public String getXML(String domain, String problem, String globalName) throws IOException {
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date date = new Date();
        String buffer;
        String episodes = "";
        int i;
        
        buffer = "<EXPERIMENT>";
        
        String value = this.getValue("HORIZON");
        
        if (value != null) {
            buffer += "<NAME>" + globalName + "(" + value  + ")</NAME>";
        }
        else {
            buffer += "<NAME>" + globalName + "</NAME>";
        }
        
        buffer += "<DATE>" + sdf.format(date) + "</DATE>";
        buffer += "<DOMAIN>" + domain + "</DOMAIN>";
        buffer += "<PROBLEM>" + problem + "</PROBLEM>";
        buffer += "<EXECUTEDACTIONS>" + this.executedActions + "</EXECUTEDACTIONS>";
        buffer += "<REPLANING>" + this.replanning + "</REPLANING>";
        buffer += "<RESULT>" + this.result + "</RESULT>";
        
        if (this.planningEpisodes.size() > 0)
            buffer += "<FIRSTPLANNINGTIME>" + this.planningEpisodes.get(0).getTime() + "</FIRSTPLANNINGTIME>";
        else
            buffer += "<FIRSTPLANNINGTIME>0.00</FIRSTPLANNINGTIME>";            
            
        buffer += "<TOTALTIME>" + this.getTime() + "</TOTALTIME>"; 
        
        for (i = 0; i < this.values.size(); i++) {
            buffer += this.values.get(i).getXML(); 
        }

        buffer += "<EPISODES>";
        for (i = 0; i < this.planningEpisodes.size(); i++) {
            buffer += this.planningEpisodes.get(i).getXML();
            episodes += (i == this.planningEpisodes.size()-1) ? this.planningEpisodes.get(i).getTime():this.planningEpisodes.get(i).getTime()+this.SEPARATOR;
        }
        buffer += "</EPISODES>";
        buffer += "<SEQUENCE>" + episodes + "</SEQUENCE>";
        buffer += "</EXPERIMENT>";
        
        return buffer;
    }
    
    public int getNumActions() {
        return this.executedActions;
    }
    
    public double getTime() {
        return (this.time_end - this.time_start) / 1000.0;
    }
    
    public double getTimeMiliseconds() {
        return (this.time_end - this.time_start);
    }
    
    public double getPlanningTime(int position) {
        if (this.planningEpisodes.size() > 0) {
            return this.planningEpisodes.get(position).getTime();
        }
        else {
            return 0.00;
        }
    }
    
    public int getReplanningEpisodes() {
        return this.replanning;
    }
}