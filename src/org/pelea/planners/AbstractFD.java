/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.planners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.pelea.core.configuration.configuration;
import org.pelea.planner.plan.Plan;
import org.pelea.planner.Planner;
import org.pelea.planner.plan.PlanAction;
import org.pelea.utils.Util;
import xml2pddl.XML2PDDL;

/**
 *
 * @author moises
 */
public class AbstractFD extends Planner 
{   
    private String[] _temp_files;
    private String horizon;
    private final boolean useFile;
    private final String abstractionFile;
    
    @SuppressWarnings("empty-statement")
    public AbstractFD(String id) {
        super(id, "AFD", false);
        
        this._result_files = new String[1];
        this._temp_files = new String[4];
        this._result_file_base = "sas_plan";
        this._result_files[0] = this._result_file_base;
        this._temp_files = new String[]{"output.sas", "output", "plan_numbers_and_cost"};
        this.useFile = Boolean.parseBoolean(configuration.getInstance().getParameter(name, "USE_FILE"));
        this.abstractionFile = (this.useFile) ? configuration.getInstance().getParameter(name, "ABSTRACTION_FILE"):"abstractions.sas";
    }

    public String getPlan(String domainH, String problemH) throws Exception {
        String command          = "";
        String result           = "";
        int i                   = 0;
        
        String domain_path  = this.temp + "domain.pddl";
        String problem_path = this.temp + "problem.pddl";
        
        // Convertimos a ppdl el dominio
        
        try 
        {
            if ((configuration.getInstance().getParameter("GENERAL", "TEMPORAL") != null) && (configuration.getInstance().getParameter("GENERAL", "TEMPORAL").equals("YES"))) 
            {
                this.createFile(XML2PDDL.convertDomainTemporal(domainH), domain_path);
                this.createFile(XML2PDDL.convertProblemTemporal(problemH), problem_path);
            }
            else 
            {
                this.createFile(XML2PDDL.convertDomain(domainH), domain_path);
                this.createFile(XML2PDDL.convertProblem(problemH), problem_path);
            }
            
            Util.printDebug(this.getName(), "Generating temporal files by domain and problem in PDDL");
        } 
        catch (Exception e) 
        {
            result += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            result += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            result += "<error>";
            result += "<type>3</type>";
            result += "<message>Domain or problem could not be converted into PDDL</message>";
            result += "</error>";
            result += "</define>";
            
            return result;
        }
        
        if (this.createFileExecution(domain_path, problem_path, 1))
        {
            try
            {
                if (this.execCommand(this.getComand(domain_path, problem_path, 1), this._result_files))
                {
                    Util.printDebug(this.getName(), "Plan generated correctly");

                    result += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                    result += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";

                    for (i = 0; i < this._result_files.length; i++)
                    {
                        Plan plan = this.getPlanInfo(this._result_files[i]);
                        plan.setTime(this.time);
                        
                        result += "<plans name=\"xPddlPlan\" domain=\" " + XML2PDDL.getNameDomain(domainH) + "\">";
                        result += plan.generateXML(domainH, problemH);
                        result += "</plans>";
                    }

                    result += "</define>";
                }
                else
                {
                    result += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                    result += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
                    result += "<error>";
                    result += "<type>1</type>";
                    result += "<message>No solution founded</message>";
                    result += "</error>";
                    result += "</define>";
                }

            }
            catch (IOException e)
            {
                result += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                result += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
                result += "<error>";
                result += "<type>9</type>";
                result += "<message>No plan files founded</message>";
                result += "</error>";
                result += "</define>";
            }
            catch (Exception e) {
                System.out.println("SUPER ERRRRRROORORORORORORORO");
            }

            new File(domain_path).delete();
            new File(problem_path).delete();
            //new File(this.temp + "execution.sh").delete();

            for (i = 0; i < this._result_files.length; i++) 
            {
                new File(this.getResultFile(i)).delete();
            }
            
            for (i = 0; i < this._temp_files.length; i++) 
            {
                new File(this._temp_files[i]).delete();
            }
        }
        else
        {
            result += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            result += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            result += "<error>";
            result += "<type>12</type>";
            result += "<message>Error execution planning process</message>";
            result += "</error>";
            result += "</define>";
        }
        
        return result;
    }
    
    @Override
    public String getPlanH(String domain, String problem) throws Exception {
        return this.getPlan(domain, problem);
    }
    
    @Override
    public String getRePlanH(String domain, String problem, String time) throws Exception 
    {
        return this.getPlan(domain, problem);
    }

    @Override
    public String getComand(String domainH, String problem, int type) throws Exception 
    {
        String command = "";
        
        if (System.getProperty("os.name").indexOf( "win" ) >= 0) {
            command = "Windows does not supported";
        }
        else {
            command = "sh " + this.temp + "execution.sh";      
        }
        
        return command;
    }
    
    private boolean createFileExecution(String domainH, String problem, int type)
    {
        PrintWriter writer = null;
        
        System.out.println("Horizon: " + this.horizon);
        
        try 
        {
            writer = new PrintWriter(new FileWriter(this.temp + "execution.sh"));
            writer.println("#! /bin/bash");
            writer.println("");
            writer.println("ulimit -t " + this.maxTime);
            
            if (this.useFile) {
                writer.println(this.path + "translate/translate.py " + domainH + " " + problem);
                writer.println("cat " + this.abstractionFile + " >> output.sas");
                writer.println(this.path + "preprocess/preprocess --use-abstractions < output.sas");
                writer.println(this.path + "search/downward --heuristic \"hff=ffabs()\" --search \"lazy_greedy([hff], preferred=[hff])\" --abstractions " + this.horizon + " < output");
            }
            else {
                if (this.mode == type) {
                    writer.println("rm " + this.path + "abstractions.sas");
                    writer.println(this.path + "translate/translate.py " + domainH + " " + problem);
                    writer.println(this.path + "preprocess/preprocess < output.sas");
                    writer.println(this.path + "search/downward --heuristic \"hlm=lmcount(lm_exhaust(cost_type=NORMAL, reasonable_orders=false, only_causal_landmarks=false, disjunctive_landmarks=true, conjunctive_landmarks=true, no_orders=false, lm_cost_type=NORMAL),pref=true)\" --generate-lm-abstractions < output");
                    writer.println("cat " + this.abstractionFile + " >> output.sas");
                    writer.println(this.path + "preprocess/preprocess --use-abstractions < output.sas");
                    writer.println(this.path + "search/downward --heuristic \"hff=ffabs()\" --search \"lazy_greedy([hff], preferred=[hff])\" --abstractions " + this.horizon + " < output");
                }
                else {
                    writer.println(this.path + "translate/translate.py " + domainH + " " + problem);
                    writer.println("cat " + this.abstractionFile + " >> output.sas");
                    writer.println(this.path + "preprocess/preprocess --use-abstractions < output.sas");
                    writer.println(this.path + "search/downward --heuristic \"hff=ffabs()\" --search \"lazy_greedy([hff], preferred=[hff])\" --abstractions " + this.horizon + " < output");
                }
            }
                
            writer.close();
            return true;
        
        } 
        catch (IOException ex) 
        {
            writer.close();
            return false;
        }
    }
    
    public void setHorizon(String horizon) {
        this.horizon = horizon;
    }
    
    public String getHorizon() {
        return this.horizon;
    }

    @Override
    public Plan getPlanInfo(String fileName) {
        Plan plan               = new Plan();
        BufferedReader bf       = null;
        String cadena           = null;
        String[] elements       = null;
        int order               = 0;
        
        try
        {
            bf = new BufferedReader(new FileReader(fileName));
            
            while ((cadena = bf.readLine())!=null) 
            {   
                elements = cadena.substring(1, cadena.length()-1).split(" ");
                
                PlanAction pa = new PlanAction(elements[0], order, 1);
                
                for (int i = 1; i < elements.length; i++)
                {
                    pa.addValue(elements[i]);
                }
                
                plan.addAction(pa);
                
                order++;
            }
            
            return plan;
        } 
        catch (FileNotFoundException ex) 
        {
            Util.printError(this.getName(), "File " + fileName + " not found");
            return null;
        } 
        catch (IOException ex) 
        {
            Util.printError(this.getName(), "Reading file " + fileName);
            return null;
        }
    }
}