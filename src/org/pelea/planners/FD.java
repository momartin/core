/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.planners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.pelea.core.configuration.configuration;
import org.pelea.planner.plan.Plan;
import org.pelea.planner.Planner;
import org.pelea.planner.plan.PlanAction;
import org.pelea.utils.Util;
import xml2pddl.XML2PDDL;

/**
 *
 * @author moises
 */
public class FD extends Planner {   
    
    private String[] tempFiles;
    
    @SuppressWarnings("empty-statement")
    public FD(String id) {
        super(id, "FD", false);
        
        this._result_files          = new String[1];
        this.tempFiles            = new String[]{"output.sas", "output", "plan_numbers_and_cost"};
        this._result_file_base      = "sas_plan";
        this._result_files[0]       = this._result_file_base;
    }
    
    @Override
    public String getPlanH(String domainH, String problemH) throws Exception {
       
        String result = super.getPlanH(domainH, problemH);
        
        for (int i = 0; i < this.tempFiles.length; i++) {
            new File(this.tempFiles[i]).delete();
        }
        
        return result;
    }
    
    @Override
    public String getRePlanH(String domainH, String problem, String time) throws Exception {
        return this.getPlanH(domainH, problem);
    }

    @Override
    public String getComand(String domainH, String problem, int type) throws Exception {
        if (System.getProperty("os.name").contains("win")) {
            return "Windows does not supported";
        }
        else {
            return "sh " + this.temp + "execution.sh";      
        }
    }
    
    private boolean createFileExecution(String domainH, String problem, int type) {
        PrintWriter writer = null;
        
        try {
            writer = new PrintWriter(new FileWriter(this.temp + "execution.sh"));
            
            writer.println("#! /bin/bash");
            writer.println("");
            writer.println("ulimit -t " + this.maxTime);
            
            writer.println(this.path + "translate/translate.py " + domainH + " " + problem);
            writer.println(this.path + "preprocess/preprocess < output.sas");
            
            if (type == 1)
                writer.println(this.path + "search/downward --heuristic \"hlm=lmcount(lm_rhw(reasonable_orders=true,lm_cost_type=2,cost_type=2),pref=true)\"  --heuristic \"hff=ff()\" --search \"lazy_greedy([hlm, hff], preferred=[hff, hlm])\"  < output");
            else if (type == 2)
                writer.println(this.path + "search/downward --heuristic \"hlm=lmcount(lm_rhw(reasonable_orders=true,lm_cost_type=2,cost_type=2),pref=true)\"  --heuristic \"hff=ff()\" --search \"lazy_greedy([hlm, hff], preferred=[hff, hlm])\"  < output");
            else if (type == 3)
                writer.println(this.path + "search/downward --heuristic \"hlm=lmcount(lm_rhw(reasonable_orders=true,lm_cost_type=2,cost_type=2),pref=true)\"  --heuristic \"hff=ff()\" --search \"lazy_greedy([hlm, hff], preferred=[hff, hlm])\"  < output");
            
            writer.close();
            
            return true;
        
        } 
        catch (IOException ex) {
            writer.close();
            return false;
        }
    }

    @Override
    public Plan getPlanInfo(String fileName) {
        Plan plan               = new Plan();
        String cadena           = null;
        String[] elements       = null;
        int order               = 0;
        
        try {
            BufferedReader bf = new BufferedReader(new FileReader(fileName));
            
            while ((cadena = bf.readLine())!=null) 
            {   
                elements = cadena.substring(1, cadena.length()-1).split(" ");
                
                PlanAction pa = new PlanAction(elements[0], order, 1);
                
                for (int i = 1; i < elements.length; i++)
                {
                    pa.addValue(elements[i]);
                }
                
                plan.addAction(pa);
                
                order++;
            }
            
            bf.close();
            
            return plan;
        } 
        catch (FileNotFoundException ex) {
            Util.printError(this.getName(), "File " + fileName + " not found");
            return null;
        } 
        catch (IOException ex) {
            Util.printError(this.getName(), "Reading file " + fileName);
            return null;
        }
    }
}