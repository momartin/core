/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.planners;

import java.io.BufferedReader;
import org.pelea.planner.plan.Plan;
import org.pelea.planner.Planner;
        
/**
 *
 * @author moises
 */
public class AbstractMFF extends Planner
{
    public AbstractMFF(String id)
    {
        super(id, "ABSTRACT-METRIC-FF", true);
        this._result_files      = new String[1];
        this._result_files[0]   = "plan_final.tmp";
        this._result_file_base  = "plan_finel.tmp";
    }

    @Override
    public String getComand(String domain, String problem, int type) throws Exception 
    {
        String command = "";
        
        //Win command
        if (System.getProperty("os.name").indexOf( "win" ) >= 0)
        {
            command = "echo \"No windows command\"";
        }
        else //Linux command
        {
            command = this.path + "ff -o " + domain + " -f " + problem;      
        }
        
        return command;
    }

    @Override
    public String getRePlanH(String domainH, String problem, String time) throws Exception 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
