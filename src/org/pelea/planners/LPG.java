/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.planners;

import java.io.BufferedReader;
import java.util.List;
import org.pelea.planner.plan.Plan;
import org.pelea.planner.Planner;
import org.pelea.utils.Util;

/**
 *
 * @author moises
 */
public class LPG extends Planner
{
    private int numFiles = 3;
    
    public LPG(String id)
    {
        super(id, "LPG-ADAPT", true);
        this._result_files      = new String[numFiles];
        this._result_file_base  = "plan_finel.tmp";
        
        for (int i = 0; i < numFiles; i++)
            this._result_files[i]   = this._result_file_base + "." + (i+1);
    }

    @Override
    public String getComand(String domain, String problem, int type) throws Exception 
    {
        String command = "";
        
        //Win command
        if (System.getProperty("os.name").indexOf( "win" ) >= 0)
        {
            command = "echo \"No windows command\"";
        }
        else //Linux command
        {
            command = this.path + " -o " + domain + " -f " + problem;      
        }
        
        return command;
    }

    @Override
    public String getRePlanH(String domainH, String problem, String time) throws Exception 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Plan getPlanInfo(String File) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
