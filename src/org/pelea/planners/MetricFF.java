/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.planners;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.pelea.planner.plan.Plan;
import org.pelea.planner.plan.PlanAction;
import org.pelea.planner.Planner;
import org.pelea.utils.Util;
        
/**
 *
 * @author moises
 */
public class MetricFF extends Planner
{
    public MetricFF(String id)
    {
        super(id, "METRIC-FF", true);
        
        this._result_files      = new String[1];
        this._result_file_base  = this.temp + "plan_final.tmp";
        this._result_files[0]   = this._result_file_base;
    }

    @Override
    public String getComand(String domain, String problem, int type) throws Exception {
        if (System.getProperty("os.name").contains("win")) {
            return "echo \"No windows command\"";
        }
        else {
            return this.path + "ff -o " + domain + " -f " + problem;      
        }
    }

    @Override
    public String getRePlanH(String domainH, String problem, String time) throws Exception 
    {
        return this.getPlanH(domainH, problem);
    }
    
    @Override
    public Plan getPlanInfo(String fileName) 
    {
        Plan plan               = new Plan();
        String cadena           = "";
        String temp             = "";
        BufferedReader bf       = null;
        int position            = 0;
        int auxiliar            = 0;
        int order               = 0;

        char character;
        
        Boolean proccesingPlan  = false;
        
        try
        {
            bf = new BufferedReader(new FileReader(fileName));
            
            while ((cadena = bf.readLine())!=null) 
            {
                proccesingPlan = (proccesingPlan || cadena.contains("0:")) ? true:false;

                if (proccesingPlan)
                {
                    position = cadena.indexOf(":");
                    
                    System.out.println(cadena);
                    
                    if (position > 0) {                  
                        PlanAction action = null;
                     
                        position++;
                        
                        while (position < cadena.length())
                        {
                            character = cadena.charAt(position);
                            
                            if ((character != '(') && (character != ')'))
                            {
                                if (character == ' ')
                                {
                                    if (!"".equals(temp))
                                    {
                                        if (action != null) {action.addValue(temp);}
                                        else {action = new PlanAction(temp, order); order++;}
                                    
                                        temp = ""; 
                                    }
                                }        
                                else
                                    temp += character;
                            }        
                            
                            position++;
                        }
                        
                        if (!temp.isEmpty()) {
                            action.addValue(temp);
                            temp = "";
                        }
                        
                        plan.addAction(action);
                    }
                    else 
                    {
                        proccesingPlan = false;
                    } 
                }
                else
                {
                    if (cadena.contains("total time")) {
                        auxiliar = cadena.indexOf("seconds");
                        plan.setTime(Double.parseDouble(cadena.substring(0, auxiliar).trim()));
                    }
                }
            }
            
            bf.close();
            
            return plan;
        } 
        catch (FileNotFoundException ex) 
        {
            Util.printError(this.getName(), "File " + fileName + " not found");
            return null;
        } 
        catch (IOException ex) 
        {
            Util.printError(this.getName(), "Reading file " + fileName);
            return null;
        }
    }
}

