/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.core.communication.connector;

import java.util.List;
import org.pelea.core.communication.Message;

/**
 *
 * @author moises
 */
public abstract class Connector
{
    public static byte SERVER = 1;
    public static byte CLIENT = 2;
    public static byte BOTH = 3;
    
    protected int port;
    protected String host;
    protected byte type;
    protected byte mode;
    
    public int getPort() {
        return this.port;
    }
    
    public String getHost() {
        return this.host;
    }
    
    public byte getType() {
        return this.type;
    }
    
    public byte getMode() {
        return this.mode;
    }
    
    public abstract int sendMessage(Message msg);
    public abstract int sendMessage(Message msg, List<String> names);
    public abstract void sendGlobalMessage(Message msg);
}
