package org.pelea.core.communication.RMI;


import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import org.pelea.core.communication.Message;

/**
 * @author Moises Martinez
 * @group PLG Universidad Carlos III
 * @version 1.0
 */

public class implementationSlave extends UnicastRemoteObject implements interfaceSlave {

    private MailBox _buffer; 
    
    public implementationSlave () throws RemoteException 
    {
        this._buffer = new MailBox();
    }

    @Override
    public synchronized void sendMessage(Message msg) throws RemoteException 
    {
        this._buffer.insertMessage(msg);
    }
    
    @Override
    public synchronized Message getMessage() throws RemoteException 
    {
       return this._buffer.getMessage();
    }
    
    public boolean messages()
    {
        return (!this._buffer.empty());
    }

}
