package org.pelea.core.communication.RMI;



import java.rmi.Remote;
import java.rmi.RemoteException;
import org.pelea.core.communication.Message;

/**
 * @author Moises Martinez
 * @group PLG Universidad Carlos III
 * @version 1.0
 */

public interface interfaceSlave extends Remote 
{
    public void sendMessage(Message msg) throws RemoteException; 
    public Message getMessage() throws RemoteException;
}
