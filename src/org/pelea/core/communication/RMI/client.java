/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.core.communication.RMI;

/**
 *
 * @author moises
 */
public class client 
{
    private final interfaceSlave _interface;
    private final byte _type;
    private final String _name;
    
    public client(byte type, String name, interfaceSlave intSlave)
    {
        this._interface = intSlave;
        this._type = type;
        this._name = name;
    }
    
    public byte getType()
    {
        return this._type;
    }
    
    public String getName()
    {
        return this._name;
    }
    
    public interfaceSlave getInterface()
    {
        return this._interface;
    }
}
