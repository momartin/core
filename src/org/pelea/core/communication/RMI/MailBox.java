package org.pelea.core.communication.RMI;


import java.util.ArrayList;
import org.pelea.core.communication.Message;

/**
 * @author Moises Martinez
 * @group PLG Universidad Carlos III
 * @version 1.0
 */
public class MailBox 
{
    private final ArrayList <Message> tail;

    public MailBox () 
    {
        tail = new ArrayList();
    }

    public synchronized void insertMessage (Message m) 
    {
        synchronized (MailBox.class) 
        {
            this.tail.add(m);
        }
    }

    public synchronized Message readMessage () 
    {
        synchronized (MailBox.class) 
        {
            return this.tail.get(0);
        }
    }

    public synchronized Message getMessage () 
    {
        synchronized (MailBox.class) 
        {
            return this.tail.remove(0);
        }
    }

    public boolean empty () 
    {
        return this.tail.isEmpty();
    }

    public int getNumberMessages()
    {
        return this.tail.size();
    }

}
