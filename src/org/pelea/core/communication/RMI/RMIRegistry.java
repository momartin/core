/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.core.communication.RMI;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author moises
 */
public class RMIRegistry 
{
    private String host;
    private int port;
    private Registry registry;
    
    public RMIRegistry(int port) throws RemoteException
    {
        this.port = port;
        this.registry = LocateRegistry.getRegistry(port);
    }
    
    public RMIRegistry(String host, int port) throws RemoteException
    {
        this.port = port;
        this.host = host;
        this.registry = LocateRegistry.getRegistry(host, port);
    }
    
    public void run() throws RemoteException
    {
        this.registry = LocateRegistry.createRegistry(this.port);
        //this.registry.list();
    }
    
    public void registerNode(String url, implementationMaster node) throws RemoteException {
        this.registry.rebind(url, node);
    }
    
    public Remote registerNode(String url) throws RemoteException, NotBoundException {
        return this.registry.lookup(url);
    }
}
