/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.core.nodes;

import java.io.StringReader;
import java.lang.management.ManagementFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.pelea.core.communication.Message;
import org.pelea.core.communication.connector.Connector;
import org.pelea.utils.Util;

/**
 *
 * @author moises
 */
public abstract class Module {
    
    public static final long TIME = 2000;
    
    public static byte RUNNING = 1;
    public static byte WAITING = 2;
    public static byte STOPPING = 3;
    public static byte RESTARTING = 4;
    
    protected long synchronizationTime;
    protected String name;
    protected String prefix;
    protected byte mode;
    protected volatile byte state;
    protected Connector commumicationModel;
    protected String PID;
    protected String masterPID;
    protected long maxTimeExecution;
    protected long startExecutionTime;
    
    
    public class ShuttingDownHandler extends Thread {
        
        public void run(Connector communicationModel) { 
            System.out.println("Control-C caught. Shutting down..."); 
            state = Module.STOPPING;
        }
    }
    
    public Module(String name, String prefix, byte mode){
        Runtime.getRuntime().addShutdownHook(new ShuttingDownHandler());
        this.PID = ManagementFactory.getRuntimeMXBean().getName();
        this.mode = mode;
        this.name = name;
        this.prefix = prefix;
    }
    
    public Module(String name, String prefix, byte mode, String masterPID) {
        Runtime.getRuntime().addShutdownHook(new ShuttingDownHandler());
        this.PID = ManagementFactory.getRuntimeMXBean().getName();
        this.masterPID = masterPID;
        this.mode = mode;
        this.name = name;
        this.prefix = prefix;
    }
    
    public String getPID() {
        return this.PID;
    }
    
    public String getMasterPID() {
        return this.masterPID;
    }
    
    public void setMasterPID(String value) {
        this.masterPID = value;
    }
    
    public String getName() {   
        return this.name;
    }
    
    public void setName(String name) {   
        this.name = name;
    }
    
    public byte getMode() {
        return this.mode;
    }
    
    public long getSynchronizationTime() {
        return this.synchronizationTime;
    }
    
    public boolean error(String content) throws Exception {
        
        if (content != null) {
            XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
            SAXBuilder builder = new SAXBuilder();
            Document document = (Document) builder.build(new StringReader(content));
            Element error = (Element) document.getRootElement().getChildren().get(0);
            
            if (error.getName().toUpperCase().matches("ERROR")) {
                Util.printError(this.getName(), outputter.outputString(error));
                return true;
            }  
        }
        return false;
    }
    
    public long getMaxTimeExecution() {
        return this.maxTimeExecution;
    }
    
    public boolean isRunning() {return (this.state == RUNNING);}
    public boolean isWaiting() {return (this.state == WAITING);}
    public boolean isStopping() {return (this.state == STOPPING);}
    public boolean isRestarting() {return (this.state == RESTARTING);}
    
    public abstract void run(long cycle);
    public abstract void syncronize();
    public abstract Message messageHandler(Message recieve) throws Exception;
    public abstract void errorHandler() throws Exception;
}
