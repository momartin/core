/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.core.nodes.RMI;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import org.pelea.core.communication.Message;
import org.pelea.core.communication.Messages;
import org.pelea.core.communication.connector.Connector;
import org.pelea.core.communication.connector.RMIConnector;
import org.pelea.core.configuration.configuration;
import org.pelea.core.nodes.Module;
import org.pelea.utils.Util;
import org.pelea.utils.experimenter.Experiment;

/**
 *
 * @author moises
 */
public abstract class RMIModule extends Module
{
    public RMIModule(String name, String prefix, byte mode) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException
    {
        super(name, prefix, mode);
        int port = Integer.parseInt(configuration.getInstance().getParameter("GENERAL", "PORT"));
        String host = configuration.getInstance().getParameter("GENERAL", "IP");
        this.commumicationModel = (RMIConnector) new RMIConnector(host, port, mode, (byte) Byte.parseByte(configuration.getInstance().getParameter(name, "type")), name);
    }
    
    public RMIModule(String name, String prefix, byte mode, String masterPID) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException
    {
        super(name, prefix, mode, masterPID);
        int port = Integer.parseInt(configuration.getInstance().getParameter("GENERAL", "PORT"));
        String host = configuration.getInstance().getParameter("GENERAL", "IP");
        this.commumicationModel = (RMIConnector) new RMIConnector(host, port, mode, (byte) Byte.parseByte(configuration.getInstance().getParameter(name, "type")), name);
    }
    
    @Override
    public void syncronize() {

        if (((RMIConnector) this.commumicationModel).getMode() == Connector.SERVER) {
        
            Util.printDebug(this.getName(), "Waiting client nodes");
        
            while (!((RMIConnector)this.commumicationModel).isSyncronized()) {
                try
                {
                    Thread.sleep((long) 100);
                }
                catch (InterruptedException ie)
                {
                    Util.printError(this.getName(), "Waiting client nodes (" + ie.toString() + ")");
                }
            }
           
            ((RMIConnector)this.commumicationModel).sendGlobalMessage(new Message(this.commumicationModel.getType(), this.name, Messages.MSG_START, null));
        }
    }
    
    @Override
    public void run(long cycle) {
       
        Message recieve = null;
        Message send;
        
        this.state = Module.WAITING;
        this.startExecutionTime = System.currentTimeMillis();
        
        try {
            
            while (!this.isStopping()) {
            
                if (((RMIConnector) this.commumicationModel).messages()) {
                    recieve = ((RMIConnector) this.commumicationModel).getMessage();
                    
                    Util.printDebug(this.getName(), recieve, 2);
                    
                    if (!this.error(recieve.getContent())) {
                        send = this.messageHandler(recieve);
                        if (send != null) ((RMIConnector) this.commumicationModel).sendMessage(send);
                    }
                    else {
                        this.errorHandler();
                    }
                }
                
                try {
                    Thread.sleep(cycle);
                }
                catch (InterruptedException ie) {
                    Util.printError(this.getName(), "Waiting loop time (" + ie.toString() + ") .");
                } 
            }
            
            if (this.commumicationModel.getMode() == Connector.CLIENT)
                ((RMIConnector) this.commumicationModel).sendUnRegisterMessage(this.name);        
        } 
        catch (RemoteException ex) {
            Util.printError(this.getName(), "Recieving message.");
        }
        catch (Exception ex) {
            Util.printError(this.getName(), ex.toString());
        }
        
        if (this.commumicationModel.getMode() == Connector.SERVER)
            ((RMIConnector)this.commumicationModel).sendGlobalMessage(new Message(this.commumicationModel.getType(), this.name, Messages.MSG_STOP, null));
    }
}
