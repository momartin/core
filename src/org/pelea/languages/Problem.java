/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages;

/**
 *
 * @author moises
 */
public abstract class Problem implements Cloneable
{
    protected String problemName;
    protected String domainName;
    
    public abstract String generateProblemXML(boolean complete);
    public abstract String generateStateXML(boolean complete);
    public abstract boolean goalsReached();
    
    /**
     * Modify name of the problem
     * @param name
     */
    public void setProblemName(String name) {
        this.problemName = name;
    }
    
    /**
     * Return name of the problem
     * @return
     */
    public String getProblemName() {
        return this.problemName;
    }
    
    /**
     * Change name of the domain
     * @param name
     */
    public void setDomainName(String name) {
        this.domainName = name;
    }
    
    /**
     * Return name of the domain
     * @return
     */
    public String getDomainName() {
        return this.domainName;
    }
    
    public String getName() {
        return this.problemName;
    }
}