/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.languages.pddl.comparison;

import org.pelea.languages.pddl.structures.NodeList;
import org.pelea.languages.pddl.structures.NodeTree;

/**
 *
 * @author moises
 */
public class EffectsPDDLComparison extends PDDLComparison {

    public EffectsPDDLComparison() {
        this.setType(PDDLComparison.EFFECTS);
    }
    
    @Override
    public boolean compare(NodeList generatedPredicates, NodeList observedPredicates) {
    
        if ((generatedPredicates != null) && (observedPredicates != null)) {
            return observedPredicates.compare(generatedPredicates);
        }
        
        return false;
    }

    @Override
    public boolean compare(NodeList genPredicates, NodeTree genGoals, NodeList obsPredicates, NodeTree obsGoals) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
