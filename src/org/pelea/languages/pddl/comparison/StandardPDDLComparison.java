/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.comparison;

import org.pelea.languages.pddl.structures.NodeList;
import org.pelea.languages.pddl.structures.NodeTree;

/**
 *
 * @author moises
 */
public class StandardPDDLComparison extends PDDLComparison 
{
    public StandardPDDLComparison() {
        this.setType(PDDLComparison.TOTAL);
    }
    
    @Override
    public boolean compare(NodeList generatedPredicates, NodeList observedPredicates) 
    {
        if ((generatedPredicates != null) && (observedPredicates != null)) {
            return generatedPredicates.compare(observedPredicates);
        }
        
        return false;
    }

    @Override
    public boolean compare(NodeList generatedPredicates, NodeTree generatedGoals, NodeList observedPredicates, NodeTree observedGoals) 
    {
        if ((generatedPredicates != null) && (observedPredicates != null))
        {
            if (generatedPredicates.compare(observedPredicates))
            {
                return generatedGoals.compare(observedGoals);
            }            
            else
                return false;
        }
        
        return false;
    }
}
