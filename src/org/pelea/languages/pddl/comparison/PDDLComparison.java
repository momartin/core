/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.comparison;

import org.pelea.languages.pddl.structures.NodeList;
import org.pelea.languages.pddl.structures.NodeTree;

/**
 *
 * @author moises
 */
public abstract class PDDLComparison 
{
    public static int TOTAL = 1;
    public static int PARTIAL = 2;
    public static int EFFECTS = 3;
    
    private int type;
    
    public void setType(int type) {
        this.type = type;
    }
    
    public int getType() {
        return this.type;
    }
    
    public abstract boolean compare(NodeList genPredicates, NodeList obsPredicates);
    public abstract boolean compare(NodeList genPredicates, NodeTree genGoals, NodeList obsPredicates, NodeTree obsGoals);
}
