/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.comparison;

import org.pelea.languages.pddl.structures.NodeList;
import org.pelea.languages.pddl.structures.NodeTree;

/**
 *
 * @author moises
 */
public class StrictPDDLComparison extends PDDLComparison 
{
    public StrictPDDLComparison() {
        this.setType(PDDLComparison.TOTAL);
    }
    
    @Override
    public boolean compare(NodeList generatedPredicates, NodeList observedPredicates) {
        if ((generatedPredicates != null) && (observedPredicates != null)) {
            if (generatedPredicates.size() == observedPredicates.size()) {
                return generatedPredicates.compare(observedPredicates);
            }
            else
                return false;
        }
        
        return false;
    }

    @Override
    public boolean compare(NodeList generatedPredicates, NodeTree genGoals, NodeList observedPredicates, NodeTree obsGoals) {
        if ((generatedPredicates != null) && (observedPredicates != null)) {
            if (generatedPredicates.size() == observedPredicates.size()) {
                if (generatedPredicates.compare(observedPredicates)) {
                    return genGoals.compare(obsGoals);
                }            
                else
                    return false;
            }
            else
                return false;
        }
        
        return false;
    }
}
