/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl;

/**
 *
 * @author moises
 */
public class UtilPDDL 
{
    public static final int NODE_NOT_DEFINED = 0;
    public static final int NODE_PREDICATE = 1;
    public static final int NODE_AND = 2;
    public static final int NODE_OR = 3;
    public static final int NODE_FOR = 4;
    public static final int NODE_NOT = 5;
    public static final int NODE_FUNCTION = 6;
    public static final int NODE_PREDICATE_ATOM = 7;
    public static final int NODE_ACTION = 8;
    public static final int NODE_EXIST = 9;
    
    public static int getType(String type, String nodeName)
    {
        if (nodeName.toUpperCase().trim().matches("GD"))
        {
            if (type.toUpperCase().trim().matches("PREDICATE"))
                return NODE_PREDICATE;
            if (type.toUpperCase().trim().matches("AND"))
                return NODE_AND;
            if (type.toUpperCase().trim().matches("OR"))
                return NODE_OR;
            if (type.toUpperCase().trim().matches("FOR"))
                return NODE_FOR;
            if (type.toUpperCase().trim().matches("NOT"))
                return NODE_NOT;
            if (type.toUpperCase().trim().matches("BINARY"))
                return NODE_FUNCTION;  
        }
        
        if (nodeName.toUpperCase().trim().matches("ATOM"))
            return NODE_PREDICATE_ATOM;
        
        if (nodeName.toUpperCase().trim().matches("EFFECT"))
        {
            if (type.toUpperCase().trim().matches("PREDICATE"))
                return NODE_PREDICATE;
            if (type.toUpperCase().trim().matches("AND"))
                return NODE_AND;
            if (type.toUpperCase().trim().matches("OR"))
                return NODE_OR;
            if (type.toUpperCase().trim().matches("FOR"))
                return NODE_FOR;
            if (type.toUpperCase().trim().matches("NOT"))
                return NODE_NOT;
            if (type.toUpperCase().trim().matches("BINARY"))
                return NODE_FUNCTION;  
        }
        
        return NODE_NOT_DEFINED;
    }

}
