/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.predicates;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author moises
 */
public class PredicateList 
{
    private List<Predicate> _predicates;

    public PredicateList()
    {
        this._predicates = new ArrayList<Predicate>();
    }

    public int getSize()
    {
        return this._predicates.size();
    }

    public void addPredicate(Predicate item)
    {
        this._predicates.add(item);
    }

    public List<Predicate> getpredicatesByName(String name)
    {
        if (!name.isEmpty())
        {
            List<Predicate> items = new ArrayList<Predicate>();

            for (int i = 0; i < this.getSize(); i++)
            {
                if (this._predicates.get(i).getName().matches(name))
                    items.add(this._predicates.get(i));
            }

            return items;
        }

        return null;
    }

    public List<Predicate> getpredicatesByValue(String value)
    {
        if (!value.isEmpty())
        {
            List<Predicate> items = new ArrayList<Predicate>();

            for (int i = 0; i < this.getSize(); i++)
            {
                if (this._predicates.get(i).getName().matches(value))
                    items.add(this._predicates.get(i));
            }

            return items;
        }

        return null;
    }
    
    public List<Predicate> get_predicates(String name, List<String> values)
    {
        return null;
    }
    
    public String geteXml()
    {
        String code = "";
        
        code += "<init>";
        
        for (int i = 0; i < this._predicates.size(); i++)
        {
            code += this._predicates.get(i).getXML();
        }
        
        code += "</init>";
        
        return code;
    }
}
