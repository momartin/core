/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.predicates;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author moises
 */
public class Predicate
{
    private String _name;
    private List<String> _values;

    public Predicate()
    {
        this._values = new ArrayList<String>();
    }

    public Predicate(String name)
    {
        this._name   = name;
        this._values = new ArrayList<String>();
    }

    public void addValue(String values)
    {
        this._values.add(values);
    }

    public void addName(String name)
    {
        this._name = name;
    }

    public String getName()
    {
    return this._name;
    }

    public boolean haveValue(String value)
    {
        int contador = 0;

        while (contador < this._values.size())
        {
            if (this._values.get(contador).matches(value))
                return true;

            contador++;
        }

        return false;
    }
    
    public String getXML()
    {
        String code = "";
        
        code += "<atom predicate=\"" + this._name +"\">";
	
        for (int i = 0; i < this._values.size(); i++)
            code += "<term name=\"" + this._values.get(i) + "\"/>"; 
	
        code += "</atom>"; 
        
        return code;
    }
}