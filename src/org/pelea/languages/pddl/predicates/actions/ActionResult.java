/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.languages.pddl.predicates.actions;

import org.pelea.languages.pddl.structures.nodes.Fluent;
import org.pelea.languages.pddl.structures.nodes.Node;
import org.pelea.languages.pddl.structures.nodes.Predicate;

/**
 *
 * @author moises
 */
public class ActionResult {

    public static int DELETED = 1;
    public static int ADDED = 2;
    public static int NUMERIC = 3;
    public static int EFFECT = 4;
    
    private int type;
    private Node element;
    
    
    public ActionResult(Predicate p, int type) {
        this.type = type;
        this.element = p;
    }
    
    public ActionResult(Fluent p, int type) {
        this.type = type;
        this.element = p;
    }
    
    public int getType() {
        return this.type;
    }
    
    public Node getElement() {
        return this.element;
    }
}
