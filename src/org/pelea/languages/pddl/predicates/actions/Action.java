/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.predicates.actions;

import java.util.ArrayList;
import java.util.List;
import org.pelea.languages.pddl.structures.NodeList;
import org.pelea.languages.pddl.structures.NodeTree;
import org.pelea.languages.pddl.structures.nodes.Node;
import org.pelea.languages.pddl.structures.nodes.Predicate;

/**
 *
 * @author moises
 */
public class Action 
{
    public static int NODURATIVE = 0;
    public static int DURATIVE = 1;
    
    private final String _name;
    private final int _type;
    private final List<Parameter> _parameters;
    private NodeTree _preconditions;
    private NodeTree _effects;
    
    public Action(String name, String type) {
        this._name = name;        
        this._type = (type.toUpperCase().contains("NO-DURATIVE")) ? Action.NODURATIVE:Action.DURATIVE;
        this._parameters = new ArrayList<Parameter>();
    }
    
    public void addParameter(String name, String type) {
        this._parameters.add(this._parameters.size(), new Parameter(name, type));
    }
    
    public void addPreconditions(NodeTree node)
    {
        this._preconditions = node;
    }
    
    public void addEffects(NodeTree node)
    {
        this._effects = node;
    }
    
    public String getName()
    {
        return this._name;
    }
    
    public int getType()
    {
        return this._type;
    }
    
    public void generateParameters(String[][] values)
    {
        for (int i = 0; i < this._parameters.size(); i++) {
            values[i][0] = this._parameters.get(i).getName();
        }
    }
    
    public List<ActionResult> generateEffects(String[][] parameters, int mode, NodeList state)
    {
        List<ActionResult> predicates = new ArrayList<ActionResult>();
        this._effects.maching(parameters, predicates, mode, state);
        
        return predicates;
    }
    
    public List<ActionResult> generatePreconditions(String[][] parameters, NodeList state)
    {
        List<ActionResult> predicates = new ArrayList<ActionResult>();
        this._preconditions.maching(parameters, predicates, 3, state);
        
        return predicates;
    }
}
