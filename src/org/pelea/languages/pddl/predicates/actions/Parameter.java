/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.predicates.actions;

import org.pelea.languages.pddl.types.Type;

/**
 *
 * @author moises
 */
public class Parameter 
{
    private String _name;
    private String _type;
    
    public Parameter(String name, String type)
    {
        this._name = name.trim();
        this._type = type.trim();
    }
    
    public String getName()
    {
        return this._name;
    }
    
    public void setName(String name)
    {
        this._name = name.trim();
    }
    
    public String getType()
    {
        return this._type;
    }
    
    public void setType(String type)
    {
        this._type = type.trim();
    }
}
