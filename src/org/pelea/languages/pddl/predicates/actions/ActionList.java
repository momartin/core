/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.predicates.actions;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author moises
 */
public class ActionList 
{
    private List<Action> _actions;

    public ActionList()
    {
        this._actions = new ArrayList<Action>();
    }

    public int getSize()
    {
        return this._actions.size();
    }
    
    public void addAction(Action node)
    {
        this._actions.add(this._actions.size(), node);
    }
    
    public Action find(String name)
    {
        for (int i = 0; i < this._actions.size(); i++)
        {
            if (this._actions.get(i).getName().toUpperCase().contains(name.toUpperCase()))
                return this._actions.get(i);
        }
        
        return null;
    }
}