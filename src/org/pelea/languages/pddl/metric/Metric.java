/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.languages.pddl.metric;

import org.jdom.Element;
import org.pelea.languages.pddl.UtilPDDL;
import org.pelea.languages.pddl.structures.nodes.Node;
import org.pelea.languages.pddl.structures.nodes.Predicate;

/**
 *
 * @author moises
 */
public class Metric {
    
    public static int MINIMIZE = 1;
    public static int MAXIMIZE = 2;
    
    private int type;
    private Node node;
    
    public Metric(Element node) {
        this.type = (node.getAttributeValue("type").trim().matches("minimize")) ? Metric.MINIMIZE:Metric.MAXIMIZE;
        
        for (int i = 0; i < 1; i++) {
            
            Element gd = (Element) node.getChildren().get(i);
            
            if (gd.getAttributeValue("type").trim().matches("predicate")) {
                this.node = new Predicate((Element) gd.getChildren().get(i));
            }
        }
    }
    
    public int getType() {
        return this.type;
    }
    
    private String getTypeString() {
        if (this.type == Metric.MAXIMIZE)
            return "maximize";
        else
            return "minimize";
    }
    
    public String generateXml() {
        
        String xml = "<metric type=\"" + this.getTypeString()  + "\">"; 
 	
        if (this.node.getType() == UtilPDDL.NODE_PREDICATE) {
            xml += "<gd type=\"predicate\">";
        }
        
        xml += this.node.getXml();
        xml += "</gd>";
        xml += "</metric>";
        
        return xml;
    }
}
