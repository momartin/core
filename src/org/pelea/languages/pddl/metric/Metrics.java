/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.languages.pddl.metric;

import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;

/**
 *
 * @author moises
 */
public class Metrics {
    
    private final String problem;
    private final String domain;
    private ArrayList<Metric> metrics;
    
    public Metrics(Element node) {
        this.domain = node.getAttributeValue("domain");
        this.problem = node.getAttributeValue("problem");
        this.metrics = new ArrayList<Metric>();
    
        List<Element> nodes = node.getChildren();
        
        for (int i = 0; i < nodes.size(); i++) {
            this.metrics.add(new Metric((Element) nodes.get(i)));
        }
    }
    
    public String getXml()
    {
        String xml = "";
        
        xml += "<metrics domain=\"" + this.domain + "\" problem=\"" + this.problem + "\">";
        
        for (int i = 0; i < this.metrics.size(); i++) {
            xml += this.metrics.get(i).generateXml();
        }
        
        xml += "</metrics>";
        
        return xml;
    }
}
