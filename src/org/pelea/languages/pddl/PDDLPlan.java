/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl;

import org.pelea.languages.Plan;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.pelea.languages.pddl.plan.ActionPlan;

/**
 *
 * @author moises
 */
public class PDDLPlan extends Plan 
{
    private static final int NUMPLANS = 1;
    private final ArrayList<ActionPlan> actions;
    
    public PDDLPlan(String xml) throws JDOMException, IOException
    {
        this.actions = new ArrayList();
        this.loadPlanFromXML(xml);
    }
    
    private void loadPlanFromXML(String xml) throws JDOMException, IOException {
        
        SAXBuilder builder = new SAXBuilder();
        Document document = (Document) builder.build(new StringReader(xml));
        Element plan = (Element) document.getRootElement().getChildren().get(0);
        List plans = plan.getChildren();
        this.name = plan.getAttributeValue("domain").trim();
        
        for (int k = 0; k < 1; k++)
        {
            this.time = Double.parseDouble(((Element) plans.get(k)).getAttribute("time").getValue());
            
            List planActions = ((Element) plans.get(k)).getChildren();
            
            for (int i = 0; i < planActions.size(); i++) {
                ActionPlan a    = new ActionPlan();
                List nodes  = ((Element) planActions.get(i)).getChildren();

                for (int j = 0; j < nodes.size(); j++) {
                    a.addAction((Element) nodes.get(j));
                }
                this.actions.add(a);
            }
        }
    }
    
    @Override
    public String getActionXML(int position)
    {
        return this.actions.get(position).getXml();
    }
    
    @Override
    public ActionPlan getAction(int position)
    {
        return this.actions.get(position);
    }
    
    public void deleteActions(int position)
    {   
        while (position < this.actions.size()) {
            this.actions.remove(position);
        }
    }
    
    @Override
    public void deleteAction(int position) {
        if (position < this.actions.size()) {
            this.actions.remove(position);
        }
    }
    
    @Override
    public void deletePlan()
    {
        this.deleteActions(0);
    }
    
    @Override
    public String generateXML()
    {
        String code = "";
        
        code += "<plans name=\"xPddlPlan\" domain=\"" + this.name + "\">";
	code += "<plan time=\"" + this.time + "\">";
        
        for (int i = 0; i < this.actions.size(); i++) {
            code += this.actions.get(i).getXml();
        }
	
        code += "</plan>";
        code += "</plans>";
        
        return code;
    }
    
    public String generateXML(int position)
    {
        String code = "";
        
        code += "<plans name=\"xPddlPlan\" domain=\"" + this.name + "\">";
	code += "<plan time=\"" + this.time + "\">";
        
        for (int i = position; i < this.actions.size(); i++) {
            code += this.actions.get(i).getXml();
        }
	
        code += "</plan>";
        code += "</plans>";
        
        return code;
    }
    
    public int getNumberOfActions()
    {
        int numActions = 0;
        
        for (int i = 0; i < this.actions.size(); i++){
            numActions += this.actions.get(i).size();
        }
        
        return numActions;
    }
    
    @Override
    public String[] updatePlan(String xml) throws JDOMException, IOException {
        this.loadPlanFromXML(xml);
        return new String[]{Double.toString(this.time), Integer.toString(this.actions.size())};
    }
    
    public String generatePDDLPlan() throws IOException {
        
        String plan = "";
        
        for (int i = 0; i < this.actions.size(); i++) {
            List<String> pddlActions = this.actions.get(i).getPDDLActions();
                
            for (java.lang.String pddlAction : pddlActions) {
                plan += i + ": " + pddlAction + "\n";
            }
        }
        return plan;
    }
    
    public static String convert(String xml) throws JDOMException, IOException {
        try {
            PDDLPlan p = new PDDLPlan(xml);
            String plan = p.generatePDDLPlan();
            p.finalize();
            return plan;
        } catch (Throwable ex) {
            return "";
        }
    }
}
