/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.plan;

import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;
import org.pelea.languages.pddl.structures.NodeList;

/**
 *
 * @author moises
 */
public class ActionPlan
{
    private NodeList actions;
    
    public ActionPlan()
    {
        this.actions = new NodeList();
    }
    
    public void addAction(Element predicate)
    {
        this.actions.add(new Action(predicate));
    }
    
    public int size()
    {
        return this.actions.size();
    }
    
    public Action get(int position)
    {
        return (Action) this.actions.get(position);
    }
    
    public String getXml()
    {
        String code = "";
        
        //code += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        code += "<action-plan num_actions=\"" + this.actions.size() + "\">";
	
        for (int i = 0; i < this.actions.size(); i++)
            code += this.actions.getXml(false);
	
        code += "</action-plan>";       
        
        return code;
    }
    
    public String getXmlByPosition(int position)
    {
        String code = "";
        
        //code += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        code += "<action-plan num_actions=\"1\">";
	code += this.actions.get(position).getXml();
        code += "</action-plan>"; 
        
        return code;
    }
    
    public List getPDDLActions() {
        
        ArrayList<String> PDDLactions = new ArrayList<String>();
        
        for (int i = 0; i < this.actions.size(); i++) {
            PDDLactions.add(((Action) this.actions.get(i)).printPDDL());
        }
        
        return PDDLactions;
    }
}
