/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.types;

/**
 *
 * @author moises
 */
public class Type 
{
    private String _type;
    private String _name;

    public Type(String type, String name)
    {
        this._type   = type;
        this._name   = name;
    }
    
    public String getName()
    {
        return this._name;
    }

    public void setName(String id)
    {
        this._name = id;
    }

    public String getType()
    {
        return this._type;
    }
    
    public void setType(String type)
    {
        this._type = type;
    }
    
    public String generateXml()
    {
        return "<term name=\"" + this._name + "\" type=\"" +  this._type + "\"/>";
    }
}
