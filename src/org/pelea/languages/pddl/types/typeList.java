/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.types;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author moises
 */
public class typeList
{
    private  List<Type> _types;

    public typeList()
    {
        this._types = new ArrayList<Type>();
    }

    public int getSize()
    {
        return this._types.size();
    }

    public void addType(Type item)
    {
        this._types.add(item);
    }

    public int getNumberType(String type)
    {
        int count = 0;

        for (int i = 0; i < this._types.size(); i++)
        {
            if (this._types.get(i).getType().matches(type))
                count++;
        }

        return count;
    }

    public Type getTypeByName(String id)
    {
        int position = 0;

        while (position < this._types.size())
        {
            if (this._types.get(position).getName().matches(id))
                return this._types.get(position);
            position++;
        }

        return null;
    }

    public Type getTypeByType(String type)
    {
        int position = 0;

        while (position < this._types.size())
        {
            if (this._types.get(position).getType().matches(type))
                return this._types.get(position);
            position++;
        }

        return null;

    }
    
    public String generateXml()
    {
        String code = "";
        
        code += "<objects>";
        
        for (int i = 0; i < this._types.size(); i++)
        {
            code += this._types.get(i).generateXml();
        }
        
        code += "</objects>";
        
        return code;
    }
}
