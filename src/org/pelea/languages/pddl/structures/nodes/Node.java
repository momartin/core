/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.structures.nodes;

import java.util.List;
import org.pelea.languages.pddl.structures.NodeList;

/**
 *
 * @author moises
 */
public abstract class Node 
{
    public static final byte DYNAMIC = 1;
    public static final byte STATIC = 2;
    public static final byte NUMERIC = 3;
    
    protected int _type;
    
    public int getType()            
    {
        return this._type;
    }

    public void setType(int type)
    {
        this._type = type;
    }
    
    public abstract String getXml();
    public abstract String getName();
    public abstract Node maching(String[][] params, int node, NodeList State);
    public abstract Node clone();
    public abstract Boolean equal(Node node);
}
