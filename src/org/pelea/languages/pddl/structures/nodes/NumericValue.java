/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelea.languages.pddl.structures.nodes;

import org.jdom.Element;

/**
 *
 * @author moises
 */
public class NumericValue {
    
    private int value;
    
    public NumericValue(int value) {
        this.value = value;
    }
    
    public NumericValue(String value) {
        this.value = Integer.parseInt(value);
    }
    
    public NumericValue(Element code) {
        this.value = Integer.parseInt(code.getAttributeValue("value").trim());
    }
    
    public int getValue() {
        return this.value;
    }
    
    public void setValue(String value) {
        this.value = Integer.parseInt(value);
    }
    
    public String getXml() {
        return "<term type=\"NUMBER\" value=\"" + this.value + "\"/>";
    }
    
    public NumericValue clone() {
        return new NumericValue(this.value);
    }
    
    public boolean equal(NumericValue value) {    
        return (this.getValue() == value.getValue());
    }
    
    public void update(int value) {
        this.value += value;
    }
}
