/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.structures.nodes;

import java.util.List;
import org.jdom.Element;
import org.pelea.languages.pddl.UtilPDDL;
import org.pelea.languages.pddl.structures.NodeList;

/**
 *
 * @author moises
 */
public class Connector extends Node
{
    private String _name;
    
    public Connector(String name)
    {
        this._type = UtilPDDL.getType(name, "GD"); //TODO SOLVE THIS
        this._name = name;
    }
    
    public Connector(Element code)
    {                
        this._type = UtilPDDL.getType(code.getAttributeValue("type"), code.getName());
        this._name = code.getAttributeValue("type").toUpperCase();
    }
    
    @Override
    public String getName()
    {
        return this._name;
    }
    
    public void setName(String name)
    {
        this._name = name;
    }
    
    @Override
    public String getXml()
    {
        return "";
    }
    
    public String getHeadXml()
    {
        return "<gd type=\"" + this.getName() + "\">";
    }
    
    public String getFootXml()
    {
        return "</gd>";
    }

    @Override
    public Node maching(String[][] params, int mode, NodeList state) 
    {
        return null;
    }
    
    @Override
    public Node clone() 
    {
        return null;
    }

    @Override
    public Boolean equal(Node node) 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
