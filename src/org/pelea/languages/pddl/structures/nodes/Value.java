/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.structures.nodes;

import org.jdom.Element;

/**
 *
 * @author moises
 */
public class Value
{
    private String _value;
    private String _type;
    
    public Value(String value)
    {
        this._value = value.trim();
        this._type  = null;
    }
    
    public Value(String value, String type)
    {
        this._value = value.trim();
        this._type  = type;
    }
    
    public Value(Element code) {
        this._value = code.getAttributeValue("value").trim();
        this._type = code.getAttributeValue("type").trim();
    }
    
    public String getValue()
    {
        return this._value;
    }
    
    public void setName(String value)
    {
        this._value = value.trim();
    }
    
    public String getType()
    {
        return this._type;
    }
    
    public void setType(String type)
    {
        this._type = type.trim();
    }
    
    public String getXml()
    {
        if (this._type == null)
            return "<term name=\"" + this._value + "\"/>";
        else
            return "<term name=\"" + this._value + "\" type=\"" + this._type + "\"/>";
    }
    
    public Value clone() {
        return new Value(this._value, this._type);
    }
    
    public boolean equal(Value value) {    
        return this.getValue().toUpperCase().matches(value.getValue().toUpperCase());
    }
}
