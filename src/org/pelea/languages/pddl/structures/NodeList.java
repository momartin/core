/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages.pddl.structures;

import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;
import org.pelea.languages.pddl.structures.nodes.Fluent;
import org.pelea.languages.pddl.structures.nodes.Node;
import org.pelea.languages.pddl.structures.nodes.Predicate;
import org.pelea.languages.pddl.UtilPDDL;

/**
 *
 * @author moises
 */
public class NodeList implements Cloneable
{
    private List<Node> _Nodes;

    public NodeList()
    {
        this._Nodes = new ArrayList<Node>();
    }

    public int size()
    {
        return this._Nodes.size();
    }
    
    public Node get(int i)
    {
        return this._Nodes.get(i);
    }

    public void add(Node node)
    {
        this._Nodes.add(node);
    }
    
    public void addNode(Element code)
    {
        if (UtilPDDL.getType(code.getAttributeValue("type"), code.getName()) == UtilPDDL.NODE_FUNCTION)
            this._Nodes.add(this._Nodes.size(), new Fluent(((Element) code.getChildren().get(0))));
        else
            this._Nodes.add(this._Nodes.size(), new Predicate(code));
    }
    
    public void delete(Node node)
    {
        for (int i = 0; i < this.size(); i++)
        {
            if (this._Nodes.get(i).getType() == node.getType())
            {
                if (this._Nodes.get(i).equal(node))
                {
                    this._Nodes.remove(i);
                    break;
                }
            }
        }
    }
    
    public void update(Node node)
    {
        for (int i = 0; i < this.size(); i++) {
            if (this._Nodes.get(i).getType() == node.getType()) {
                if (((Fluent) this._Nodes.get(i)).equal((Fluent) node)) {
                    ((Fluent) this._Nodes.get(i)).updateValue(((Fluent) node).getValue());
                    break;
                }
            }
        }
    }
    
    public void delete(int position)
    {
        this._Nodes.remove(position);
    }
    
    public void updateValueNode(Node node)
    {
        for (int i = 0; i < this.size(); i++)
        {
            if (this._Nodes.get(i).getType() == node.getType()) 
            {
                //if (this._Nodes.get(i).equal(node))
                    //((Fluent) this._Nodes.get(i)).update(node);
            }
        }
    }

    public List<Node> getNodesByName(String name)
    {
        if (!name.isEmpty())
        {
            List<Node> items = new ArrayList<Node>();

            for (int i = 0; i < this.size(); i++)
            {
                if (this._Nodes.get(i).getName().matches(name))
                    items.add(this._Nodes.get(i));
            }

            return items;
        }

        return null;
    }

    public List<Node> getNodesByValue(String value)
    {
        if (!value.isEmpty())
        {
            List<Node> items = new ArrayList<Node>();

            for (int i = 0; i < this.size(); i++)
            {
                if (this._Nodes.get(i).getName().matches(value))
                    items.add(this._Nodes.get(i));
            }

            return items;
        }

        return null;
    }
    
    public List<Node> getNodesByNameAndValue(String name, String[] values)
    {
        int i, j = 0; 
        
        if (!name.isEmpty())
        {
            List<Node> items = new ArrayList();

            for (i = 0; i < this.size(); i++)
            {
                if (this._Nodes.get(i).getName().matches(name)) {
                    
                    if (this._Nodes.get(i) instanceof Predicate) { 
                    
                        String[] pValues = ((Predicate) this._Nodes.get(i)).getValues();

                        for (j = 0; j < values.length; j++) {
                            if (!pValues[j].toUpperCase().matches(values[j].toUpperCase()))
                                break;
                        }

                        if (j == values.length) {
                            items.add(this._Nodes.get(i));
                        }
                    }
                    else if (this._Nodes.get(i) instanceof Fluent) {
                    
                         Object[] nodes = ((Fluent) this._Nodes.get(i)).getValues();
                         
                         String[] pValues = ((Predicate) nodes[0]).getValues();

                         for (j = 0; j < values.length; j++) {
                            if (!pValues[j].toUpperCase().matches(values[j].toUpperCase()))
                                break;
                        }

                        if (j == values.length) {
                            items.add(this._Nodes.get(i));
                        }
                         
                    }
                }
            }

            return items;
        }

        return null;
    }
    
    public void deleteNodeByMode(byte mode)
    {
        int position = 0;
        
        while (position < this.size())
        {
            if (this._Nodes.get(position).getType() == UtilPDDL.NODE_PREDICATE)
            {
                if (((Predicate) this._Nodes.get(position)).getMode() == mode)
                    this._Nodes.remove(position);
                else
                    position++;
            }
            else if (this._Nodes.get(position).getType() == UtilPDDL.NODE_FUNCTION)
            {
                if (((Fluent) this._Nodes.get(position)).getMode() == mode)
                    this._Nodes.remove(position);
                else
                    position++;
            }
            else
                position++;
        }
    }
    
    public String getXml(boolean problem)
    {
        String code = "";
        
        if (problem)
            code += "<init>";
        
        for (int i = 0; i < this._Nodes.size(); i++)
        {
            code += this._Nodes.get(i).getXml();
        }
        
        if (problem)
            code += "</init>";
        
        return code;
    }
    
    
    public List generateCursor(int size)
    {
        ArrayList ids = new ArrayList();
        
        for (int i = 0; i < size; i++)
        {
            ids.add(new Integer(i));
        }
        
        return ids;
    }
    
    public boolean compare(NodeList list)
    {
        ArrayList cursor    = (ArrayList) this.generateCursor(this.size());
        int position        = 0;
        int cursorPosition;
        boolean find;
        
        while (position < list.size()) {
            cursorPosition = 0;
            find = false;
            
            while ((!find) && (cursorPosition < cursor.size())) {
                if (list.get(position).getType() == this.get((Integer) cursor.get(cursorPosition)).getType()) {
                    if (list.get(position).equal(this.get((Integer) cursor.get(cursorPosition)))) {
                        find = true;
                        cursor.remove(cursorPosition);
                    }
                }                
                cursorPosition++;
            }
            
            if (find)   position++;
            else {
                //System.out.println(list.get(position).getXml());
                return false;
            }
        }

        return true;
    }
    
    public void printDebug() {
        for (int i = 0; i < this.size(); i++) {
            System.out.println("(" + this._Nodes.get(i).getName() + ((Predicate) this._Nodes.get(i)).getValuesDebug() + ")");
        }
    }
}
