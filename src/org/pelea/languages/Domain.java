/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages;

/**
 *
 * @author Moises Mart�nez
 */
public abstract class Domain 
{
    protected String domainName;
    
    public abstract void loadDomain(String xml);
    public abstract String generateDomainXML(boolean complete);
    
    public final String getName() {
        return this.domainName;
    }
    
    public String getDomainName() {
        return this.domainName;
    }
    
    public final void setDomainName(String name) {
        this.domainName = name;
    }
}
