/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.languages;

import java.io.IOException;
import org.jdom.JDOMException;
import org.pelea.languages.pddl.plan.ActionPlan;

/**
 *
 * @author moises
 */
public abstract class Plan implements Cloneable
{
    protected String name;
    protected double time;
    protected int version;
    
    public abstract String getActionXML(int position);
    public abstract ActionPlan getAction(int position);
    public abstract void deleteAction(int position);
    public abstract void deletePlan();
    public abstract String generateXML();    
     public abstract String generateXML(int position); 
    public abstract int getNumberOfActions();
    public abstract String[] updatePlan(String XMLPlan) throws JDOMException, IOException;
    
    public String getName() {
        return this.name;
    }
    
    public double getPlanningTime() {
        return this.time;
    }
    
    public int getVersion() {
        return this.version;
    }

}
